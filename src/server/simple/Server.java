package server.simple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public void connect() throws IOException {
		
		ServerSocket server = new ServerSocket(1989);
		
		System.out.println("Server is readying...");
		
		Socket socket = server.accept();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		String request = br.readLine();
		if (request != null) {
			System.out.println("Received from a client: " + request);
			
			//Process: select database, calculate
			//response to client
			
			//sending
			PrintStream ps = new PrintStream(socket.getOutputStream());
			ps.println(request.toUpperCase());
			
			//server.close();
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		Server server = new Server();
		server.connect();
	}
	
}
